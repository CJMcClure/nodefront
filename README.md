# Casey McClure's Portfolio

<img style="height: 100px; width: 100px; border-radius: 100%;" src="images/casey-mcclure.jpg"/>

## NodeFront

NodeFront is an E-commerce prototype I built in 2-days, with an Inventory management backend and 
a dynamic front end. It's an ongoing project and I intend to build it into a fully functional (CMS) 
Content Management System and (IMS) Inventory Management System, for use by small business as a store
front solution. The front-end will ultimately be built in React or Angular. 

This prototype was built using **NodeJS, ExpressJS, Sequelize, Bootstrap, & Stripe API**. 
